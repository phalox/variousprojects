#include "taskFlyport.h"
#include "midi.h"

#include "pico_stack.h"
#include "pico_config.h"
#include "pico_ipv4.h"
#include "pico_socket.h"
#include "pico_icmp4.h"
#include "pico_dev_mrf24wg.h"
#include "pico_slaacv4.h"
#include "pico_zmq.h"
#include "ws2812.h"

#include <string.h>

#define ZMQ_DEMO
#define ZMQ_DEMO_NODE_PUB

// TODO: dirty hack, remove!
extern RSSI_VAL myRSSI;
extern tWFNetwork xNet;

#define BUFFER_SIZE 45
#define PRESS_TIME 300
#define DOUBLE_CLICK_TIME 1000
// Green, Red, Blue
static uint8_t buffer[BUFFER_SIZE];

static struct pico_device *wlan;

uint16_t sleep_cnt = 900;

int16_t allocs[5];
int16_t current_led = 0;
uint16_t going_up = 1;
uint64_t time_button_pressed = 0;
uint8_t led_color = COL_RED;

uint8_t game_ready = 0;

#define HELLO_PORT 666
#define HELLO_TIMEOUT 200
#define HELLO_NEIGHBOUR_LOST (HELLO_TIMEOUT * 3)
struct pico_timer *hello_timer;
struct pico_socket *hello_socket;

#define MAX_NEIGHBOUR_NODES 10
struct hello_entry
{
    struct pico_ip4 ip;
    pico_time last_heard;
    ZMQ zmq_obj;
};
struct hello_entry neighbour_nodes[MAX_NEIGHBOUR_NODES];
uint16_t hello_port_be;

#define PONG_PORT 1337
uint16_t pong_port_be;
ZMQ pong_publisher;

uint8_t udp_buf[16];
uint8_t udp_length;

enum pong_state_t {
    PONG_IDLE,
    PONG_RECV_BALL,
    PONG_GOT_BALL_DOWN,
    PONG_GOT_BALL_UP,
    PONG_SEND_BALL,
    PONG_SENT_BALL,
    PONG_HAD_BALL_LAST,
    PONG_LOST_BALL,
};

enum pong_state_t pong_state;

struct pico_ip4 my_own_ip;
struct pico_ip4 broadcast;

uint8_t send_buf[16] = {0xDE, 0xAD, 0xBE, 0xEF};
uint8_t send_buf_len = 4;
pico_time last_action = 0, last_action_timeout = 0;
uint32_t last_sent_to = 0;
uint32_t got_ball_from = 0;
uint16_t num_peers = 0;

uint8_t no_peers = 0;

#define LAST_ACTION_TIMEOUT (HELLO_TIMEOUT * 3)
#define LAST_ACTION_TIMEOUT_STOP (HELLO_TIMEOUT * 40)

void hello_timer_cb(pico_time time, void *arg)
{
    
    int ret = 0;
    int i;
    // Send out broadcast
    ret = pico_socket_sendto(hello_socket, send_buf, send_buf_len, &broadcast, hello_port_be);

    for(i = 0; i < MAX_NEIGHBOUR_NODES; i++)
    {
        /* Neighbour lost */
        if( neighbour_nodes[i].ip.addr > 0 && (neighbour_nodes[i].last_heard + HELLO_NEIGHBOUR_LOST) < PICO_TIME_MS())
        {
            neighbour_nodes[i].ip.addr = 0;
            neighbour_nodes[i].last_heard = 0;
            
            /* TODO Remove ZeroMQ connections */
            //zmq_close(neighbour_nodes[i].zmq_obj);
            //neighbour_nodes[i].zmq_obj = NULL;

            dbg("Neighbour lost, removing peer (left: %u)\n", num_peers );

            if(num_peers > 0)
            {
                num_peers--;
            }
        }
    }

    if(last_action > 0 && (last_action + LAST_ACTION_TIMEOUT) < PICO_TIME_MS())
    {
        switch(pong_state)
        {
            case PONG_RECV_BALL:
                current_led = 14;
                pong_state = PONG_GOT_BALL_DOWN;
                last_action = 0;
                last_action_timeout = 0;
                set_message("", 0);
                break;
            case PONG_SEND_BALL:
                current_led = 0;
                pong_state = PONG_GOT_BALL_UP;
                last_action = 0;
                last_action_timeout = 0;
                set_message("", 0);
                break;
            default:
                break;
        }
    }

    if(last_action_timeout > 0 && (last_action_timeout + LAST_ACTION_TIMEOUT_STOP) < PICO_TIME_MS())
    {
        if(PONG_HAD_BALL_LAST == pong_state || PONG_LOST_BALL == pong_state || PONG_SENT_BALL == pong_state)
        {
            /* Something went wrong - back to idle */
            current_led = 0;
            pong_state = PONG_IDLE;
            set_message("", 0);

            last_action = 0;
            last_action_timeout = 0;
        }
    }

    hello_timer = pico_timer_add(HELLO_TIMEOUT, hello_timer_cb, NULL);
}

void set_message(char *action, uint32_t peer)
{
    /* Resetting the message */
    if(0 == strlen(action))
    {
        send_buf_len = 4;
        return;
    }
    else
    {
        send_buf_len = 16;
        memcpy(&send_buf[4], &peer, 4);
        memcpy(&send_buf[8], action, 8);
    }
}

void pong_send_ball()
{
    uint32_t random = pico_rand();
    if(num_peers > 0)
    {

        random = random % num_peers;
        last_sent_to = neighbour_nodes[random].ip.addr;
        dbg("Sending ball to %u\n", last_sent_to);
        set_message("BALLSENT", long_be(last_sent_to));
        pong_state = PONG_SENT_BALL;
        last_action = PICO_TIME_MS();
        last_action_timeout = last_action;
        no_peers = 0;
    }
    else
    {
        dbg("No peers to send to :(\n");
        pong_state = PONG_GOT_BALL_DOWN;
        current_led = 14;
        last_action = 0;
        no_peers = 1;
    }
}
/*
void zmq_pong_cb(ZMQ z)
{
    uint8_t recvbuf[30];
    int ret = zmq_recv(z, recvbuf);
    recvbuf[29] = '\0';

    dbg("TFP Received %d bytes: \"%s\"\n", ret, recvbuf);
}
*/
void hello_cb(uint16_t ev, struct pico_socket *s)
{
    uint8_t recvbuf[400];
    char ipstr[30];
    uint32_t peer;
    uint16_t port;
    int read = 0, total_read = 0;
    int i, res;
    struct hello_entry *first_empty_hello = NULL;
    
    if(ev == PICO_SOCK_EV_RD)
    {
        do {
            read = pico_socket_recvfrom(s, recvbuf, 400, &peer, &port);
            if(read > 0)
                total_read += read;
        } while(read > 0);

        if(recvbuf[0] == 0xDE && recvbuf[1] == 0xAD &&
                recvbuf[2] == 0xBE && recvbuf[3] == 0xEF)
        {
            if(peer == my_own_ip.addr)
                return;

            /* Add entry to list */
            for(i = 0; i < MAX_NEIGHBOUR_NODES; i++)
            {
                if(first_empty_hello == NULL && neighbour_nodes[i].ip.addr == 0)
                {
                    first_empty_hello = &(neighbour_nodes[i]);
                }

                /* Found entry */
                if(neighbour_nodes[i].ip.addr == peer)
                {
                    neighbour_nodes[i].last_heard = PICO_TIME_MS();
                    first_empty_hello = NULL;
                    //dbg("Updating ip %u\n", peer);
                    break;
                }
            }
            /* We have to add a new entry */
            if(first_empty_hello != NULL)
            {
                first_empty_hello->ip.addr = peer;
                first_empty_hello->last_heard = PICO_TIME_MS();

                /* Create ZeroMQ connection to this node */
                //first_empty_hello->zmq_obj = zmq_subscriber(zmq_pong_cb);
                pico_ipv4_to_string(ipstr, peer);
                //res = zmq_connect(first_empty_hello->zmq_obj, ipstr, PONG_PORT);

                //dbg("Adding ip %u, connected to peer status: %d\n", peer, res);
                num_peers = num_peers + 1;
                dbg("Adding ip %u\n", peer);
            }
            /* Let's parse the other information */
            if(total_read == 16)
            {
                uint32_t to_addr = long_be(long_from(&recvbuf[4]));
                uint8_t *msg = &recvbuf[8];

                //dbg("ToAddr is %u\n", to_addr);

                /* It doesn't matter who sent this, it's out of my hands now */
                if(PONG_HAD_BALL_LAST == pong_state)
                {
                    /* Ball has been sent by someone else, No more control! */
                    if(!strncmp(msg, "BALLSENT", 8))
                    {
                        pong_state = PONG_IDLE;
                    }
                }

                 if(to_addr != my_own_ip.addr)
                    return;

                if(!strncmp(msg, "BALLSENT", 8))
                {
                    /* OOh nice, someone sent us the ball! */
                    last_action = PICO_TIME_MS();
                    //if(PONG_IDLE == pong_state)
                    //{
                        /* TODO Do stuff with LEDs here */
                        last_action_timeout = PICO_TIME_MS();
                        got_ball_from = peer;
                        pong_state = PONG_RECV_BALL;
                        set_message("BALLRECV", long_be(peer));
                    //}
                }
                else if(!strncmp(msg, "BALLRECV", 8))
                {
                    /* Someone got my ball! */
                    if(PONG_SENT_BALL == pong_state)
                    {
                        /* TODO Do stuff with LEDs here */
                        pong_state = PONG_HAD_BALL_LAST;
                        set_message("", 0);
                    }
                }
                else if(!strncmp(msg, "BALLLOST", 8))
                {
                    last_action = PICO_TIME_MS();
                    /* Send the ball to a new unit */
                    if(PONG_HAD_BALL_LAST == pong_state)
                    {
                        dbg("Someone lost my ball!\n");
                        last_action_timeout = PICO_TIME_MS();
                        pong_state = PONG_SEND_BALL;
                        set_message("BALLINIT", long_be(peer));
                    }
                }
                else if(!strncmp(msg, "BALLINIT", 8))
                {
                    /* The previous guy has restarted the ball */
                    if(PONG_LOST_BALL == pong_state)
                    {
                        pong_state = PONG_IDLE;
                        set_message("", 0);
                    }
                }
            }
        }
        else
        {
            dbg("Some other bytes were received - No deadbeef\n");
        }
    }
    
    if(ev == PICO_SOCK_EV_ERR)
    {
        printf("We had a socket error, leaving!\n");
        exit(1);
    }
    
}

void open_hello_socket(struct pico_ip4 *ip)
{
    hello_socket = pico_socket_open(PICO_PROTO_IPV4, PICO_PROTO_UDP, hello_cb);
    if (!hello_socket)
        exit(1);
    /* bind the socket to port_be */
    if (pico_socket_bind(hello_socket, ip, &hello_port_be) != 0)
        exit(1);

    game_ready = 1;
}
/*
void zmq_publisher_cb(ZMQ z)
{
    dbg("ZeroMQ callback called\n");
}
 */

void slaac_cb(struct pico_ip4 *ip, uint8_t code)
{
    char ipbuf[20];
    /* IP is configured */
    if(PICO_SLAACV4_SUCCESS == code)
    {
        my_own_ip.addr = ip->addr;
        pico_ipv4_to_string(ipbuf, ip->addr);
        printf("Ip configured:: %s, %u\n", ipbuf, ip->addr);

        /* Create this ZeroMQ publisher */
        //pong_publisher = zmq_publisher(PONG_PORT, zmq_publisher_cb);

        open_hello_socket(ip);
        hello_timer = pico_timer_add(HELLO_TIMEOUT, hello_timer_cb, NULL);
    }
    if(PICO_SLAACV4_ERROR == code)
    {
        printf("Failed to get an IP\n");
    }
}

void FlyportTask()
{

    unsigned long elapsed = 0;
    struct pico_ip4 hostip, routeip, hostip2;
    struct pico_ip4 mask;
    struct pico_ip4 zero={}, gateway;
    int i;

    pico_stack_init();
    UARTWrite(1, "PicoTCP Stack init...\n");
    
    wlan = (struct pico_device *) pico_mrf24wg_create("mrf24wg");
    if (!wlan) {
        IOPut(p19, on);
        while (1);
    }

    for(i = 0; i < MAX_NEIGHBOUR_NODES; i++)
    {
        neighbour_nodes[i].ip.addr = 0;
        neighbour_nodes[i].zmq_obj = NULL;
    }
    
    udp_buf[0] = 0xDE;
    udp_buf[1] = 0xAD;
    udp_buf[2] = 0xBE;
    udp_buf[3] = 0xEF;
    udp_length = 4;

    hello_port_be = short_be(HELLO_PORT);
    pong_port_be = short_be(PONG_PORT);


#define HOSTIP_1 "169.254.22.5"
#define HOSTIP_2 "169.254.22.6"

    pico_string_to_ipv4("169.254.255.255",&broadcast.addr);

    pico_string_to_ipv4(HOSTIP_1,&hostip.addr);
    pico_string_to_ipv4("255.255.0.0",&mask.addr);
    pico_string_to_ipv4(HOSTIP_2,&hostip2.addr);

    //pico_string_to_ipv4("192.168.1.100",&hostip.addr);
    //pico_string_to_ipv4("255.255.255.0",&mask.addr);

    pico_string_to_ipv4("192.168.1.0",&routeip.addr);
    pico_slaacv4_claimip(wlan, slaac_cb);

    /*
#ifndef ZMQ_DEMO
    pico_slaacv4_claimip(wlan, slaac_cb);
#else   
    #ifdef ZMQ_DEMO_NODE_PUB
    pico_ipv4_link_add(wlan, hostip, mask);
    ZMQ zmq_sock = zmq_publisher(PONG_PORT, zmq_publisher_cb);
    #else
    pico_ipv4_link_add(wlan, hostip, mask);
    ZMQ zmq_sock = zmq_subscriber(zmq_pong_cb);
    if(zmq_connect(zmq_sock, HOSTIP_2, PONG_PORT))
        dbg("Something went wrong when trying to connect");
    
    #endif
#endif
    */

    uint8_t double_pressed = 0;
    while(1)
    {
        pico_stack_tick();
        PICO_IDLE();

        if(IOButtonState(5) == PRESSED)
        {
            if(PICO_TIME_MS() < time_button_pressed + DOUBLE_CLICK_TIME)
            {
                printf("Button double_pressed\n");
                double_pressed = 1;
                if(PONG_IDLE == pong_state)
                {
                    current_led = 0;
                    pong_state = PONG_GOT_BALL_UP;
                }
            }
            time_button_pressed = PICO_TIME_MS();
            printf("Button pressed %llu\n", time_button_pressed);
        }
        if(game_ready)
        {
            if (++elapsed > 100) {
                IOPut(p19, toggle);
                elapsed = 0;
                ws2812_set_col(buffer, BUFFER_SIZE, current_led, led_color);
                if(PONG_GOT_BALL_DOWN == pong_state)
                {
                    current_led = current_led - 1;
                    if(current_led == -1)
                    {
                        current_led = current_led + 2;
                        printf("Checking led time %llu - thresh_time: %llu\n", PICO_TIME_MS(), (time_button_pressed + PRESS_TIME));
                        if(time_button_pressed + PRESS_TIME > PICO_TIME_MS())
                        {
                            /* Button was pressed at correct time */
                            led_color++;
                            if(led_color == (COL_BLEU + 1))
                                led_color = COL_GREEN;
                            pong_state = PONG_GOT_BALL_UP;
                        }
                        else
                        {
                            if(0 == no_peers)
                            {
                                /* Ball was lost, start broadcasting */
                                pong_state = PONG_LOST_BALL;
                                set_message("BALLLOST", long_be(got_ball_from));
                                last_action = PICO_TIME_MS();
                                last_action_timeout = last_action;
                                current_led = 0;
                            }
                            else
                            {
                                current_led = 0;
                                pong_state = PONG_IDLE;
                                last_action = 0;
                                set_message("", 0);
                            }
                            no_peers = 0;
                        }
                    }
                }
                else if(PONG_GOT_BALL_UP == pong_state)
                {
                    current_led = current_led + 1;

                    dbg("");

                    if(current_led == 15)
                    {
                        /* Ball is leaving strip */
                        pong_send_ball();
                    }
                }
            }
            
            /*
            #ifdef ZMQ_DEMO_NODE_PUB
            uint8_t buf[] = {0xDE, 0xAD, 0xBE, 0xEF};
            int ret = zmq_send(zmq_sock, buf, 4);
            dbg("Message sent %d\n", ret);
            #endif
             */
        }
        ws2812_send(buffer, BUFFER_SIZE);
    }
}
